#include <iostream>

#include "Array.hpp"

int main()
{
	Array<int> test(1);

	std::cout << test[0] << std::endl;
	// std::cout << test[1] << std::endl;

	ArrayStack<int> test_stack;

	std::cout << "test_stack.size(): " << test_stack.size() << std::endl;

	std::cout << "test_stack.get(0): " << test_stack.get(0) << "\n";
	std::cout << "test_stack.get(1): " << test_stack.get(1) << std::endl;
	// std::cout << "test_stack.get(10): " << test_stack.get(10) << std::endl;
}
