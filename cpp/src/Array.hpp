#ifndef ARRAY_HPP_INCLUDED
#define ARRAY_HPP_INCLUDED

#include <cassert>
#include <cstddef>

/**
 * @brief array
 */
template <typename T> class Array
{
      public:
	Array(size_t len);
	~Array();

	T &operator[](size_t i);
	Array<T> &operator=(Array<T> &b);

      private:
	T *a = nullptr;
	size_t length;
};

template <typename T> Array<T>::Array(size_t len)
{
	length = len;
	a = new T[length];
}

template <typename T> Array<T>::~Array()
{
	delete[] a;
}

template <typename T> T &Array<T>::operator[](size_t i)
{
	assert(0 <= i && i < length);
	return a[i];
}

template <typename T> Array<T> &Array<T>::operator=(Array<T> &b)
{
	if (a != nullptr) {
		delete[] a;
	}

	a = b.a;
	b.a = nullptr;
	length = b.length;

	return *this;
}

template <typename T> class ArrayStack
{
      public:
	ArrayStack() : a(DEFAULT_LENGTH){};

	size_t size();

	T get(size_t i);

      private:
	Array<T> a;
	size_t n = DEFAULT_LENGTH;

	static constexpr size_t DEFAULT_LENGTH = 10;
};

template <typename T> size_t ArrayStack<T>::size()
{
	return n;
}

template <typename T> T ArrayStack<T>::get(size_t i)
{
	return a[i];
}
#endif
